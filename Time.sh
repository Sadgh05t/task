#!/bin/bash

#Call the config file . . 
source configFile.sh

#Install ntp on the server
sudo apt-get install ntp -y

#The config files for ntp lies in /etc/ntp.conf.
#We are changing the Servers time to our nearest geo location.


sed -i "s/server 0.ubuntu.pool.ntp.org/server 1.eu.pool.ntp.org/g" /etc/ntp.conf
sed -i "s/server 1.ubuntu.pool.ntp.org/server 1.eu.pool.ntp.org/g" /etc/ntp.conf
sed -i "s/server 2.ubuntu.pool.ntp.org/server 2.eu.pool.ntp.org/g" /etc/ntp.conf
sed -i "s/server 3.ubuntu.pool.ntp.org//g" /etc/ntp.conf

#Restart the service.
sudo service ntp restart